from django.db import models


class Link(models.Model):
    """Represent a link to an article/video/podcast."""
    title = models.CharField(max_length=255)
    url = models.URLField(unique=True)
    description = models.TextField(blank=True, null=True)
    published = models.BooleanField(default=False, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("-created_at",)

    def __str__(self):
        return self.title
