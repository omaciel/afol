from django.urls import path

from afol.links.views import LinkList, LinkDetail

urlpatterns = [
    path('<int:pk>/', LinkDetail.as_view()),
    path('', LinkList.as_view()),
]