from django.contrib import admin

from afol.links.models import Link


class LinkAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "url",
        "description",
        "published",
        "created_at",
        "modified_at",
    )
    list_filter = ('published',)


admin.site.register(Link, LinkAdmin)
