import pytest

from django.db.utils import IntegrityError

from afol.links.models import Link

pytestmark = pytest.mark.django_db

def test_create_link():
    """Create Link with only required fields."""
    link = Link(
        title='Foo',
        url='https://www.example.com'
    )
    assert link.title == 'Foo'
    assert link.description is None
    assert link.published == False

def test_url_integrity_error():
    """URL is unique field for a Link."""
    link1 = Link(title='Foo', url='https://foo')
    link2 = Link(title='Foo', url='https://foo')

    link1.save()
    with pytest.raises(IntegrityError):
        link2.save()
